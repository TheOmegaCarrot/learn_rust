fn sequence_sum(start: u32, end: u32, step: u32) -> u32 {
    if start > end { 
        return 0;
    } 


    (start..=end)
        .step_by(step as usize)
        .sum()
}

fn main() {
    println!("{}", sequence_sum(5,1,3));

    println!("{}", sequence_sum(2,2,2));
    println!("{}", sequence_sum(2,6,2));
    println!("{}", sequence_sum(1,5,1));
    println!("{}", sequence_sum(1,5,3));
}
