fn is_valid_ip(ip: &str) -> bool {
    let segments = ip.split('.').collect::<Vec<&str>>();

    let right_num_segments = segments.len() == 4;

    let no_leading_zeros = !segments.iter().any(|segment| segment.starts_with('0') && (segment.len() != 1));

    let parses_as_u8 = segments
        .iter()
        .map(|segment| segment.parse::<u8>())
        .all(|result| result.is_ok());

    right_num_segments && no_leading_zeros && parses_as_u8
}

#[cfg(test)]
mod tests {
    use super::is_valid_ip;

    #[test]
    fn sample_test() {
        assert!(is_valid_ip("0.0.0.0"));
        assert!(is_valid_ip("12.255.56.1"));
        assert!(is_valid_ip("137.255.156.100"));

        assert!(!is_valid_ip(""));
        assert!(!is_valid_ip("abc.def.ghi.jkl"));
        assert!(!is_valid_ip("123.456.789.0"));
        assert!(!is_valid_ip("12.34.56"));
        assert!(!is_valid_ip("01.02.03.04"));
        assert!(!is_valid_ip("256.1.2.3"));
        assert!(!is_valid_ip("1.2.3.4.5"));
        assert!(!is_valid_ip("123,45,67,89"));
        assert!(!is_valid_ip("1e0.1e1.1e2.2e2"));
        assert!(!is_valid_ip(" 1.2.3.4"));
        assert!(!is_valid_ip("1.2.3.4 "));
        assert!(!is_valid_ip("12.34.56.-7"));
        assert!(!is_valid_ip("1.2.3.4\n"));
        assert!(!is_valid_ip("\n1.2.3.4"));
        assert!(!is_valid_ip("12.255.56.01"));
    }
}

fn main() {
    println!("Hello, world! {}", is_valid_ip("8.8.8.8"));
}
