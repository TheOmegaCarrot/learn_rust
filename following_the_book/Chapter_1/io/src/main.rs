use std::io::Write;

fn main() {
    let mut user_input = String::new();
    print!("Input a number: ");
    let _ = std::io::stdout().flush();
    std::io::stdin()
        .read_line(&mut user_input)
        .expect("Something went wrong reading input!");

    let trimmed_user_input = user_input.trim();

    let number: i32 = trimmed_user_input.parse().expect("Bad input!");
    println!("\n\"{trimmed_user_input}\" parsed into \"{number}\"");
}
