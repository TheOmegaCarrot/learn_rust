struct Thing {
    i: i32,
    d: i32
}

impl Thing {
    fn sum(&self) -> i32 {
        self.i + self.d
    }
}

fn main() {
    let thing = Thing {
        i: 42,
        d: 81
    };

    println!("Sum 1: {}, Sum 2: {}", thing.sum(), Thing{i:81, d:72}.sum());
}
